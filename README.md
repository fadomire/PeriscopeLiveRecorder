# Periscope Live Recorder

### Version
2.0.0

NodeJs application that check for periscope profiles and save their lives.

### Requried softwares
- NodeJs: https://nodejs.org/
- livestreamer: http://docs.livestreamer.io/install.html

### Installation
```sh
git clone git@git.framasoft.org:Sobralia/PeriscopeLiveRecorder.git
npm install
```

### Usage
```
node ./
```

Then open your web browser and go to this page: http://localhost:3000
to manage your profiles list.

# Changelog

## [2.0.0]
- Recode database, recorder, upload modules
- Now use ffmpeg to download Video (later it will be only used to rotate and transcode)
- Enhanced web interface
- Create logger module to manage console and log files

## [1.2.1]
- Emergency bug fixes (updated broadcast status by filename instead of id atm)
- Disabling auto refresh list on web interface
- Do not start recording if the broadcast's id didn't change

## [1.2.0]
- Video can be automatically uploaded on YouTube

## [1.1.1]
- Bug fix: Status of lives don't wait for the recordBroadcast callback to toggle, they are updated on every check

## [1.1.0]
- Added unique ID for each video file saved to avoid file existing problem
- Colored console output
- Text field for YouTube description and hastags
- Live broadcast's infos are stored in the database for future auto upload on YouTube channel

## [1.0.0]
- web interface (http://localhost:3000) to manage periscope profiles without reboot the server
