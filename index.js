require('app-module-path').addPath(__dirname + "/lib/");

var logger = require('logger');
var database = require('database');
var recorder = require('recorder');
var	web = require('web.js');
var	api = require('api.js');
var	upload = require('upload.js');
var express = require('express');
var app = express();
var router = express.Router();


logger.init();
database.reset_working_values();
recorder.init(upload);
api.init(router);
web.init(app, router, express);
upload.init(app);
