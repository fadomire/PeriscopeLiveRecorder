var google = require('googleapis');
var OAuth2 = google.auth.OAuth2;
var request = require('request');
var fs = require('fs');
var database = require('database');
var logger = require('logger');

var client_id = "69403384593-8do574nauj51tiklqo48bhptbk92u0fn.apps.googleusercontent.com";
var client_secret = "nE4fOdwpzrYD37luwzD3bZMG";
var redirect_url = "http://localhost:3000/oauth2callback";
var authClient = new OAuth2(client_id, client_secret, redirect_url);
var youtube = google.youtube({version: "v3", auth: authClient});
var isUploading = false;

module.exports = {
  tokens:  null,
  init: function(app){
    this.auth(app);
    this.loadTokens();
    if (this.tokens){
      authClient.setCredentials(this.tokens);
      this.refresh_token();
    }
    logger.info("Uploader ready")
  },
  saveTokens: function(tokens){
    fs.writeFileSync("credentials.json", JSON.stringify(tokens));
  },
  loadTokens: function(){
    var raw = fs.readFileSync("credentials.json");
    this.tokens = JSON.parse(raw);
  },
  refresh_token: function(){
    var _this = this;
    authClient.refreshAccessToken(function(err, tokens) {
      _this.tokens = tokens;
      _this.uploadVideos();
    });
  },
  auth: function(app){
    var _this = this;
    var url = authClient.generateAuthUrl({
      access_type: 'offline',
      scope: [
        "https://www.googleapis.com/auth/youtube.force-ssl",
        "https://www.googleapis.com/auth/youtube",
        "https://www.googleapis.com/auth/youtube.upload"
      ]
    });

    logger.debug("Go to: " + url);

    app.get('/oauth2callback', function(req, res){
      res.json({ message: 'Auth succes!' });
      authClient.getToken(res.req.query.code, function(err, tokens) {
        // Now tokens contains an access_token and an optional refresh_token. Save them.
        if(!err) {
          _this.tokens = tokens;
          authClient.setCredentials(tokens);
          _this.saveTokens(tokens);
          _this.uploadVideos();
        }
        else{
          logger.error(err);
        }
      });
    });
  },
  uploadVideos: function (){
    var _this = this;
    var profiles = database.get_all_profiles();
    var i = 0;

    if (!!profiles && isUploading === false){
      while (i < profiles.length && isUploading === false){
        var profile = profiles[i];
        var broadcasts = profile.broadcasts;
        var j = 0;

        if (!!profile){
          while (j < broadcasts.length && isUploading === false){
            var broadcast = broadcasts[j];

            if (!!broadcast && broadcast.upload.done === false && isUploading === false){
              logger.debug("Status: " + broadcast.upload.done + ":" + isUploading);
              logger.debug(broadcast.upload.done + ": " + broadcast.filename);
              isUploading = true;
              fs.stat(broadcast.filename, function(err, stat){
                if (err == null){
                  //Refresh token before upload a video
                  logger.info("Refresh Youtube Token");
                  authClient.refreshAccessToken(function(err, tokens) {
                    if (err){
                      logger.error(err);
                      return;
                    }
                    authClient.setCredentials(tokens);
                    _this.saveTokens(tokens);
                    _this.uploadOneVideo(profile, broadcast);
                  });
                }else{
                  logger.error(new Error("Uploader Error: File does not exist"));
                  isUploading = false;

                  var index = profile.broadcasts.map(function(e) { return e.filename; }).indexOf(broadcast.filename);
                  profile.broadcasts[index].upload.done = true;
                  database.update_profile(profile);
                  _this.uploadVideos();
                }
              });
            }
            j++;
          }
        }
        i++;
      }
    }
  },
  uploadOneVideo: function(profile, broadcast, callback){
    var _this = this;
    var start = broadcast.filename.lastIndexOf("/") + 1;
    var end = broadcast.filename.lastIndexOf(".");
    var title = broadcast.filename.substr(start, end - start);
    var profile = profile;
    var tmp_broadcast = broadcast;
    var params = {
      resource: {
        snippet: {
          title: broadcast.title,
          description: profile.description,
          tags: ["StreamDebout", "NuitDebout", "ReportersDebout"]
        },
        status: {
          privacyStatus: "private"
        }
      },
      part: "snippet,status",
      media: {
        body: fs.createReadStream(broadcast.filename)
      }
    }

    logger.info("start upload: " + title);
    youtube.videos.insert(params, function(err, res){
      if (err){
        logger.error(err);
      }
      var db_profile = database.get_profile_by_name(tmp_broadcast.raw.username);
      logger.debug(tmp_broadcast.raw.username);
      if (!!db_profile && !!db_profile.broadcasts){
        var index = db_profile.broadcasts.map(function(e) { return e.filename; }).indexOf(tmp_broadcast.filename);

        logger.info(tmp_broadcast.raw.username + " new video upload!");
        db_profile.broadcasts[index].upload.done = true;
        database.update_profile(db_profile);
      }

      setTimeout(function(){
        isUploading = false;
        _this.uploadVideos();
      }, 10000);
    });
  }
};
