var database = require('database');
var logger = require('logger');

module.exports.init = function(router){
  /**
  * Rest API
  **/
  router.use(function(req, res, next) {
      next();
  });

  router.route('/profiles')
    .get(function(req, res){
      res.json(database.get_all_profiles())
    })
    .post(function(req, res){
      var new_profile = database.new_profile();

      new_profile.name = req.body.name;
      new_profile.description = req.body.description;
      new_profile.hashtags = req.body.hashtags;
      database.save_profile(new_profile);
      res.json({ message: req.body.name + ' profile created!' });
    });

  router.route('/profiles/:profile_id')
    .delete(function(req, res){
      database.delete_profile(req.params.profile_id);
      res.json({ message: req.params.profile_id + ' profile deleted!' });
    })
    .put(function(req, res){
      var profile = database.get_profile_by_id(req.params.profile_id);

      if (profile){
        profile.name = req.body.name;
        profile.description = req.body.description;
        profile.hashtags = req.body.hashtags;
        database.update_profile(profile);
        res.json({ message: req.params.profile_id + ' profile updated!' });
      }else{
        res.json({ message: req.params.profile_id + ' can\'t update profile' });
      }
    })
    .get(function(req, res){
      var profile = database.get_profile_by_id(req.params.profile_id);
      res.json(profile);
    });

    router.route('/profiles/:profile_id/broadcasts')
      .get(function(req, res){
        var profile = database.get_profile_by_id(req.params.profile_id);
        var response = {};

        if (!!profile && !!profile.broadcasts){
          response = profile.broadcasts;
        }
        res.json(response);
      });

    router.route('/broadcasts/')
      .get(function(req, res){
        var broadcasts_list = [];
        var profiles = database.get_all_profiles();
        var profiles_length = 0;
        var i = 0;

        if (!!profiles){
          while (i < profiles_length){
              broadcasts_list = broadcasts_list.concat(profiles.broadcasts);
            i++
          }
        }
        res.json(broadcasts_list);
      });

      router.route('/logs/')
        .get(function(req, res){

        });
    logger.info("API ready" .green);
};
