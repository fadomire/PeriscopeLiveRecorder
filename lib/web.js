var bodyParser = require('body-parser');
var logger = require('logger');

module.exports.init = function(app, router, express){
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.get('/', function (req, res) {
    var	options = {
    	root: __dirname + "/web/",
    	dotfiles: 'deny',
    	headers: {
    		"x-timestamp": Date.now(),
    		"x-sent": true
    	}
    };
    res.sendFile("index.html", options, function(err){
    	if (err) {
    	  logger.error(err);
    	  res.status(err.status).end();
    	}
    });
  });

  // Static routes for css, js, pictures, etc..
  app.use("/static", express.static("node_modules/bootstrap/dist"));
  app.use("/static/js", express.static("node_modules/angular"));
  app.use("/static/js", express.static("node_modules/angular-resource"));
  app.use("/static/js", express.static("node_modules/angular-route"));
  app.use("/static/js", express.static("node_modules/jquery/dist"));
  app.use("/static/js", express.static("lib/web/js"));
  app.use("/static", express.static("lib/web"));
  app.use("/partials", express.static("lib/web/partials"));
  // API route
  app.use('/api', router);

  // Launch server
  app.listen(3000, function () {
    logger.info('WebServer listening on port 3000!'.green);
  });
};
