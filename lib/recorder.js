var database = require('database');
var logger = require('logger');
var	shortid = require('shortid');
var	fs = require('fs');
var	periscope = require('periscope-api');
var exec = require('child_process').exec;

module.exports = {
  upload: null,
  init: function(upload){
    this.upload = upload;
    this.check_profiles();
    logger.info("Recorder ready");
  },
  create_directories: function(){
    //Create Video and Database folder if not exist
    if (!fs.existsSync(__dirname + "/live")){
      fs.mkdirSync(__dirname + "/live");
    }if (!fs.existsSync(__dirname + "/db")){
      fs.mkdirSync(__dirname + "/db");
    }
  },
  create_profile_directory: function(profile_name){
    var	path = __dirname + "/live/" + profile_name;

    if (!fs.existsSync(path)){
      fs.mkdirSync(path);
    }
  },
  check_profiles: function(){
    var profiles_list = database.get_all_profiles();
    var profiles_length = profiles_list.length;
    var _this = this;

    for (var i = 0; i < profiles_length; i++){
      var the_profile = profiles_list[i];
      var name = the_profile.name;

      periscope.getProfileInfo(name, function(err, profile){
        if (err){
          logger.error(err);
          return;
        }
        if (profile !== "undefined" && _this.profile_is_live(profile)){
          logger.info(profile.info.username + " just started a live");
          _this.record_broadcast(profile);
        }
      });
    }

    setTimeout(function(){
      _this.check_profiles();
    }, 10000);
  },
  profile_is_live: function(profile){
    var is_live = false;
    if (!!profile && !!profile.info && !!profile.info.username){
      var db_profile = database.get_profile_by_name(profile.info.username);

      if (!!db_profile){
        var db_profile_broadcasts_length = db_profile.broadcasts.length;

        if (!!profile.lastBroadcast && profile.lastBroadcast.state === "RUNNING"){
          var aleady_recording = (db_profile.last_broadcast_ids.indexOf(profile.lastBroadcast.id) == -1) ? false : true;
          if (!aleady_recording){
            is_live = true;
            this.toggle_profile({name: profile.info.username}, true, profile.lastBroadcast.id);
          }
        }
      }
    }

    return (is_live);
  },
  get_filename: function(profile_name){
    var date = new Date();
    var	dateString = date.toLocaleDateString();
    var	timeString = date.toLocaleTimeString();
    var	path = __dirname + "/live/" + profile_name + "/"
    var	localfilename = profile_name + "_" + dateString + "_" + timeString.replace(/:/g, "-") + "_" + shortid.generate() + ".mp4";
    var	filename = {
      localfilename: path + localfilename,
      title: profile_name + " " + dateString + " " + timeString
    }
    return (filename);
  },
  record_broadcast: function(profile){
    var filename = this.get_filename(profile.info.username);
    var _this = this;
    var broadcast_id = profile.lastBroadcast.id;
    var username = profile.info.username;

    this.create_profile_directory(username);
    logger.info("start recording: " + filename.localfilename);
    this.record_broadcast2(broadcast_id, filename.localfilename, function(err, broadcast_info){
      var info = null;
      var db_profile = database.get_profile_by_name(username);
      var new_broadcast = database.new_broadcast();

      if (err){
        logger.error(err);
      }else{
        info = broadcast_info.broadcast;
      }
      new_broadcast.raw = info;
      new_broadcast.filename = filename.localfilename;
      new_broadcast.title = filename.title;
      new_broadcast.upload.ready = true;
      db_profile.broadcasts.push(new_broadcast);
      database.update_profile(db_profile);

      //Delete broadcast from last_broadcast_ids
      _this.toggle_profile({name: username}, false, profile.lastBroadcast.id);
      logger.info(username + " ended live");
      logger.info("File saved: " + filename.localfilename);
      _this.upload.uploadVideos();
    });
  },
  record_broadcast2: function(broadcast_id, path, callback){
    periscope.getBroadcastInfo(broadcast_id, function(err, broadcasts_info){
      if (err){
        callback(err);
        return;
      }

      var	replay_url = broadcasts_info.hls_url;
      var transpose = " -vf transpose=dir=0 ";
      var	cmd = "ffmpeg -loglevel -8 -i " + replay_url + " -c copy -bsf:a aac_adtstoasc " + path;
      logger.debug(replay_url);

      fs.stat(path, function(err, stat){
        if (err == null){
          callback(new Error("File already exist"));
          return;
        }
        if (err && err.code != "ENOENT"){
          callback(err);
          return;
        }
        exec(cmd, function(err, stdout, stderr) {
          if (err){
            callback(err, broadcasts_info);
            return;
          }
          callback(null, broadcasts_info);
        });
      });
    })
  },
  toggle_profile: function(options, is_live, last_broadcast_id){
    var profile = null;

    if (options.id){
      profile = database.get_profile_by_id(options.id);
    }else if (options.name){
      profile = database.get_profile_by_name(options.name);
    }
    profile.is_live = is_live;
    if (is_live === false){
      var index = profile.last_broadcast_ids.indexOf(last_broadcast_id);
      profile.last_broadcast_ids.splice(index, 1);
    }else{
      profile.last_broadcast_ids.push(last_broadcast_id);
    }
    database.update_profile(profile);
  }
}
