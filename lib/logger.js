var fs = require('fs');
var Log = require('log');
var colors = require('colors');

module.exports = {
  log: null,
  read: null,
  filename: "",
  init: function(){
    var _this = this;
  	var date = new Date();
  	var	dateString = date.toLocaleDateString();
  	var	timeString = date.toLocaleTimeString();

    this.filename = "log/" + dateString + "-" + timeString.replace(/:/g, "-") + ".txt";
    this.log = new Log('debug', fs.createWriteStream(this.filename, {
      flags: 'a'
    }));
    this.info("Logger ready");
  },
  debug: function(line){
    console.log(colors.blue(line));
    this.log.debug(line);
  },
  error: function(line){
    console.log(colors.red(line));
    this.log.error(line);
  },
  info: function(line){
    console.log(colors.green(line));
    this.log.info(line);
  }
};
