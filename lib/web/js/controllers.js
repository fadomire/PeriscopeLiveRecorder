var periliveCtrl = angular.module('periliveCtrl', []);

periliveCtrl.controller('settingsCtrl',[
	'$scope',
	function($scope){

	}
]);

periliveCtrl.controller('profilesCtrl', ['$scope', '$interval', 'Profile', function($scope, $interval, Profile){
	$scope.profile = new Profile();
	$scope.profiles = Profile.query();

	$scope.setActive = function(profile){
		$scope.activeProfile = profile;
		$scope.editProfile(profile);
	}

	$scope.refresh = function(){
		var	tmp_profiles = Profile.query({}, function(){
			var	length = tmp_profiles.length;
			var	i = 0;

			for (i = 0; i < length; i++){
				var	index = $scope.profiles.map(function(e) { return e._id; }).indexOf(tmp_profiles[i]._id);
				if (index == -1){
					$scope.profiles.push(tmp_profiles[i]);
				}else{
					$scope.profiles[index] = tmp_profiles[i];
				}
			}
		});
	};

	$scope.deleteProfile = function(profile){
		Profile.delete(profile, function(){
	    $scope.refresh();
		});
	};

	$scope.newProfile = function(){
		$scope.profile = new Profile();
	};

	$scope.editProfile = function(profile){
		$scope.profile = angular.copy(profile);
	};

	$scope.saveProfile = function(profile){
		if($scope.profile._id)
		{
			Profile.update({_id: $scope.profile._id}, $scope.profile);
		}else{
			$scope.profile.$save();
		}
	  $scope.refresh();
	};

	$scope.cancelEdit = function(){
		$scope.activeProfile = null;
		$scope.profile = new Profile();
	};

	$scope.intervalPromise = $interval(function(){
	    $scope.refresh();
	}, 10000);
}]);
