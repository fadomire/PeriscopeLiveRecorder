var perilive = angular.module('perilive', [
  'ngRoute',
  'periliveCtrl',
  'periliveServices'
]);

perilive.config(['$routeProvider', function($routeProvier){
  //Système de routage
  $routeProvier
    .when("/login", {
      templateUrl : 'partials/login.html',
      controller: 'loginCtrl'
    })
    .when("/logout", {
      templateUrl : 'partials/logout.html',
      controller: 'logoutCtrl'
    })
    .when("/home", {
      templateUrl : 'partials/dashboard.html',
      controller: 'homeCtrl'
    })
    .when("/profiles", {
      templateUrl : 'partials/profiles.html',
      controller: 'profilesCtrl'
    })
    .when("/profiles/:id", {
      templateUrl : 'partials/one_profile.html',
      controller: 'profilesCtrl'
    })
    .when("/settings", {
      templateUrl : 'partials/settings.html',
      controller: 'settingsCtrl'
    })
    .when("/videos", {
      templateUrl : 'partials/videos.html',
      controller: 'videosCtrl'
    })
    .otherwise({
      redirectTo: '/dashboard'
    })
}]);
