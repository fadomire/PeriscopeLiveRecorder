var	periliveServices = angular.module("periliveServices", ["ngResource"]);

periliveServices.factory('Profile', ['$resource', function($resource){
    return $resource('/api/profiles/:_id', {
    	name: '@name',
    	id: '@_id',
    	islive: '@islive',
    	description: '@description',
    	hashtags: '@hashtags'
    }, {
    	update: {
    		method: 'PUT'
    	}
    });
  }]);
